from selenium.webdriver.common.by import By

class Locators():
    # Login
    LOGIN_URL = r"https://auth.clubspark.uk/account/signin?ReturnUrl=%2fissue%2fwsfed%3fwa%3dwsignin1.0%26wtrealm%3dhttps%253a%252f%252fclubspark.lta.org.uk%252f%26wctx%3drm%253d0%2526id%253d0%2526ru%253dhttps%25253a%25252f%25252fclubspark.lta.org.uk%25252fQueensParkTennisCourts%26wct%3d2020-06-29T18%253a50%253a32Z%26prealm%3dhttps%253a%252f%252fclubspark.lta.org.uk%252f%26proot%3dhttps%253a%252f%252fclubspark.lta.org.uk%252f%26paroot%3dhttps%253a%252f%252fclubspark.lta.org.uk%252fQueensParkTennisCourts%26source%3dQueensParkTennisCourts%26name%3dQueen%2527s%2bPark%2bTennis%2bCourts%26nologo%3d0&wa=wsignin1.0&wtrealm=https%3a%2f%2fclubspark.lta.org.uk%2f&wctx=rm%3d0%26id%3d0%26ru%3dhttps%253a%252f%252fclubspark.lta.org.uk%252fQueensParkTennisCourts&wct=2020-06-29T18%3a50%3a32Z&prealm=https%3a%2f%2fclubspark.lta.org.uk%2f&proot=https%3a%2f%2fclubspark.lta.org.uk%2f&paroot=https%3a%2f%2fclubspark.lta.org.uk%2fQueensParkTennisCourts&source=QueensParkTennisCourts&name=Queen%27s+Park+Tennis+Courts&nologo=0"
    LOGIN_BOX=(By.XPATH, '//*[@id="EmailAddress"]')
    # CAPTCHA = (By.XPATH, '/html/body/div[1]/div[5]/div/div[2]/form/table/tbody/tr/td/img')
    # CAPTCHA_INPUT = (By.NAME, 'captcha')
    PASSWORD_BOX=(By.XPATH, '//*[@id="Password"]')
    SUBMIT_BOX=(By.XPATH, '//*[@id="signin-btn"]')
    SUCCESSFUL_LOGIN = (By.XPATH, '//*[@id="account-options"]/a/span')
    BOOKING_URL = 'https://clubspark.lta.org.uk/QueensParkTennisCourts/Booking/BookByDate#?date={date}&role=member'
    SESSION_TIME = 'available-booking-slot' 
    COURT_NAME = './/h3'
    COURT_BLOCK = (By.CLASS_NAME, 'resource-wrap')
    SELECTED_DATE = (By.CLASS_NAME, 'pull-left')
    SESSION_TIME_REGEX = r'Book at (?P<start_time>\d{2}:\d{2}) - (?P<end_time>\d{2}:\d{2})'
