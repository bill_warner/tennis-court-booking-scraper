# Tennis Court Booking Scraper
## Scope
* Scraper designed to harvest data on local tennis court availability. This data is provided by [Club Spark](https://clubspark.lta.org.uk/QueensParkTennisCourts).
* Scraper written using Selenium in conjunction with Chromedriver. 

## Future improvements:
* Host script on AWS to run remotely.
* Set up email notifications to notify users when a court becomes available on their chosen day/time.
* Create autopayment function.