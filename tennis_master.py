from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from locators import Locators as locators
from datetime import datetime, timedelta
import requests
import json
import os
import re

CHROME_EXECUTABLE_PATH = os.path.expanduser(r'~/projects/selenium_webdriver/83/chromedriver')
OCR_API_KEY = 'ENTER API KEY'
# Clubspark Login Details
USERNAME = 'ENTER EMAIL'
PASSWORD = 'ENTER PASSWORD'

class SetUpBase():
    def setUp(self):
        chrome_options=webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('window-size=1920x1080')
        self.driver=webdriver.Chrome(CHROME_EXECUTABLE_PATH, options=chrome_options)

class BasePage():
    """This class is the parent class for all the pages in our application."""
    """It contains all common elements and functionalities available to all pages."""

    # this function is called every time a new object of the base class is created.
    def __init__(self, driver):
        self.driver=driver

    # this function performs click on web element whose locator is passed to it.
    def click(self, by_locator):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator)).click()
    
    # this function asserts comparison of a web element's text with passed in text.
    def assert_element_text(self, by_locator, element_text):
        web_element=WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
        assert web_element.text == element_text

    # this function performs text entry of the passed in text, in a web element whose locator is passed to it.
    def enter_text(self, by_locator, text, clear_text = True):
        if clear_text:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator)).clear()

        return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator)).send_keys(text)
    # this function checks if the web element whose locator has been passed to it, is enabled or not and returns
    # web element if it is enabled.
    def is_enabled(self, by_locator):
        return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))

    # this function checks if the web element whose locator has been passed to it, is visible or not and returns
    # true or false depending upon its visibility.
    def is_visible(self,by_locator):
        element=WebDriverWait(self.driver, 1).until(EC.visibility_of_element_located(by_locator))
        return bool(element)

    def return_element(self,by_locator):
        element=WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
        return element

    def return_elements(self,by_locator):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
        elements=self.driver.find_elements(*by_locator)
        return elements

    # this function moves the mouse pointer over a web element whose locator has been passed to it.
    def hover_to(self, by_locator):
        element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
        ActionChains(self.driver).move_to_element(element).perform()

    def check_element_exists(self, by_locator):
        try:
            self.driver.find_element(*by_locator)
        except NoSuchElementException:
            return False
        return True

class LoginPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(locators.LOGIN_URL)
        self.sucessfully_logged_in = False

    '''CAPTCHA decoding. Saves CAPTCHA image, send to OCR API and returns string.
       Works for simple CAPTCHAs. Sometimes takes a few attempts.
       Not currently being used here but leaving in for future'''
    def save_captcha(self):
        img = self.return_element(locators.CAPTCHA)
        with open('captcha.png', 'wb') as file:
            file.write(img.screenshot_as_png)

    def decode_captcha(self, filename, overlay=False, api_key=OCR_API_KEY, language='eng'):
        payload = {'isOverlayRequired': overlay,
                   'apikey': api_key,
                   'language': language,
                   'OCREngine':2
                   }
        with open(filename, 'rb') as f:
            r = requests.post('https://api.ocr.space/parse/image',
                              files={filename: f},
                              data=payload,
                              )
        payload = json.loads(r.content.decode())
        self.captcha_answer = payload['ParsedResults'][0]['ParsedText']

    def login_form(self, username, password):
        '''Fills login form with username and password'''
        self.enter_text(locators.LOGIN_BOX, username)
        self.enter_text(locators.PASSWORD_BOX, password)
        self.click(locators.SUBMIT_BOX)
        if self.check_element_exists(locators.SUCCESSFUL_LOGIN):
                self.sucessfully_logged_in = True
                print('Successfully Logged In')
        else:
            print('Failed Log In')

class Slot(object):
    def __init__(self, court_number, session_start_time, session_end_time, session_date):
        self.court_number = court_number
        self.session_start_time = session_start_time
        self.session_end_time = session_end_time
        self.session_date = session_date

class BookingPage(BasePage):
    def __init__(self, driver, date):
        super().__init__(driver)
        self.date = date
        self.driver.get(locators.BOOKING_URL.format(date = self.date))

    def date_suffix(self, day_of_month):
        '''Returns day of month with appropriate suffix'''
        suffix = ['st', 'nd', 'rd', 'th']
        exceptions = [11, 12, 13]
        remainder = day_of_month % 10
        if day_of_month not in exceptions and remainder < 4:
            return str(day_of_month) + suffix[remainder-1]
        else:
            return str(day_of_month) + suffix[3]

    def expected_date(self):
        '''Returns the class date in a Tues 1st Jun format'''
        date = datetime.strptime(self.date, '%Y-%m-%d').date()
        day_of_month = datetime.strftime(date, '%d')
        day_of_month = self.date_suffix(int(day_of_month))
        week_day = datetime.strftime(date, '%A')
        month = datetime.strftime(date, '%B')
        expected_date = f'{week_day} {day_of_month} {month}'
        return expected_date

    def date_check(self, by_locator):
        '''Force driver to wait for the input date to match on-site date'''
        desired_date = self.expected_date()
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element(by_locator, desired_date)
            )

    def regex_group_return(self, pattern, string, group):
        match = re.search(pattern,string)
        return match.group(group)

    def session_interval_extraction(self, session_interval, start_end_time):
        '''Returns time from time string'''
        time = self.regex_group_return(
            pattern = locators.SESSION_TIME_REGEX,
            string = session_interval,
            group = start_end_time
            )
        time = datetime.strptime(time, '%H:%M').time()
        return time

    def get_slots(self):
        '''Returns all available slots for a given day'''
        slots = []
        court_blocks = self.return_elements(locators.COURT_BLOCK)
        for court_block in court_blocks:
            court_name = court_block.find_element_by_xpath(locators.COURT_NAME).text
            if 'Court' not in court_name:
                continue
            court_number = int(court_name.split('Court ')[1])
            session_times = court_block.find_elements_by_class_name(locators.SESSION_TIME)
            session_date = datetime.strptime(self.date, '%Y-%m-%d').date()
            for session_time in session_times:
                session_interval = session_time.get_attribute('innerHTML')
                session_start_time = self.session_interval_extraction(
                    session_interval = session_interval,
                    start_end_time = 'start_time'
                    )
                session_end_time = self.session_interval_extraction(
                    session_interval = session_interval,
                    start_end_time = 'end_time'
                    )
                slot = Slot(court_number, session_start_time, session_end_time, session_date)
                slots.append(slot)
        return slots

class TennisSlots(SetUpBase):
    def __init__(self):
        super().__init__()

    def login(self):
        login=LoginPage(self.driver)
        while not login.sucessfully_logged_in:
            login.login_form(USERNAME, PASSWORD)

    def date_generator(self, num_days):
        todays_date = datetime.now().date()
        dates = [datetime.strftime(todays_date + timedelta(days=x), format = '%Y-%m-%d') for x in range(num_days)]
        return dates
        
    def main(self):
        self.setUp()
        self.login()
        dates = self.date_generator(9)
        slots = []
        for date in dates:
            booking_page = BookingPage(self.driver, date)
            booking_page.date_check(locators.SELECTED_DATE)
            slots += booking_page.get_slots()
        slots.sort(key=lambda x:(x.session_date, x.session_start_time))
        for slot in slots:
            session_date = datetime.strftime(slot.session_date,'%a %d %b %Y')
            print(f'{session_date}, {slot.session_start_time} - {slot.session_end_time}, Court: {slot.court_number}')
        self.driver.quit()

if __name__ == '__main__':
    TennisSlots().main()
